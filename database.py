import sqlite3

# connect to a database / temporary data
# this is useful to test stuff and does not stand after the program is off
# conn = sqlite3.connect(':memory:')

# connect to a database / permanent data
conn = sqlite3.connect('customer.db')

# create a cursor
c = conn.cursor()

def create_customers():
    c.execute("""CREATE TABLE customers (
        first_name TEXT,
        last_name TEXT,
        email TEXT
        )""")
    conn.commit()

def insert_customers():
    many_customers = [
        ('Ana', 'Pereira', 'ana@gata.com'),
        ('Joana', 'Dark', 'joana@dark.com'),
        ('Pedro', 'Barros', 'pedro@234it.com'),
        ('Ayrton', 'Senna', 'f1@champ.f1')
    ]
    c.executemany("INSERT INTO customers VALUES (?, ?, ?)", many_customers)
    conn.commit()

def show_all():
    c.execute("SELECT rowid, * FROM customers")
    items = c.fetchall()
    for item in items:
        print(item)

def add(first, last, mail):
    c.execute("INSERT INTO customers VALUES(?, ?, ?)", (first, last, mail))
    conn.commit()

def add_many(list):
    c.executemany("INSERT INTO customers VALUES(?, ?, ?)", (list))
    conn.commit()

def delete(id):
    c.execute("DELETE FROM customers WHERE rowid = (?)", id)
    conn.commit()

def search_by_id(id):
    record = c.execute("SELECT rowid, * FROM customers WHERE rowid = (?)", str(id))    
    result = c.fetchall()
    return result

# close connection
# conn.close()
