# Our App

This project was made with Udemy classes to learn sqlite with Python
https://www.udemy.com/course/using-sqlite3-databases-with-python/learn/lecture/14445796#overview

* Make sure you have Python Installed (recommended version > 3)

## Getting started

git clone https://gitlab.com/jacky_cupcakes/our_app

cd our_app

python main.py

That should work

